# Install OpenFaaS on Kind

### Prereqs
 - Docker [Install](https://docs.docker.com/get-started/)
 
 - Kind [Install](https://kind.sigs.k8s.io/docs/user/quick-start/)
 
 - Kubectl [Install](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
 
 - OpenFaas CLI
 
   **Linux**
   
 	```sh
 	curl -sLSf https://cli.openfaas.com | sudo sh
 	```
 	**Mac**
 	
 	```sh
 	brew install faas-cli
 	```
 	
 #### Create the kind config
 
 **kind-cluster.yml**
 
 ```sh
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
- role: worker
- role: worker
 ```
 
 #### Start the kind cluster
 
 ```sh
 kind create cluster --name primary --config primary-cluster.yml
 ```
 
 #### Create the namespaces
 
 ```sh
 kubectl apply -f https://raw.githubusercontent.com/openfaas/faas-netes/master/namespaces.yml
 ```
 
 #### Add and Update the Helm Repository
 
 ```sh
 helm repo add openfaas https://openfaas.github.io/faas-netes && helm repo update    
 ```
 or

 ```sh
kubectl create namespace openfaas
kubectl create namespace openfaas-fn
 ```
 
 #### Install OpenFaaS on K8s
 
 ```sh
 helm upgrade openfaas --install openfaas/openfaas --namespace openfaas --set basic_auth=false --set functionNamespace=openfaas-fn --set operator.create=true
 ```
 
 #### Check the OpenFaaS Status
 
 ```sh
 kubectl --namespace=openfaas get deployments -l "release=openfaas, app=openfaas"
 ```
 
 #### Port Forward the UI
 
 ```sh
 kubectl port-forward svc/gateway -n openfaas 8080:8080
 ```
 
 !!!Note: For the CLI and port forward you can set the cli variable like this and port above to match.  For instance if you are already running a service on 8080.
 
 ```sh
 OPENFAAS_URL=http://127.0.0.1:9090
 ```
 
 #### Deploy a Function
 
 ```sh
 faas-cli store deploy figlet
 echo "Hello, $USER" | faas-cli invoke figlet
 ```
 
 #### List Your Function  
 
 ```sh
 kubectl get functions -n openfaas-fn
 ```

#### Get the URL for the Function

```sh
faas-cli describe figlet
```

#### Curl the function

```sh
curl -X POST http://127.0.0.1:8080/function/figlet -d "Hello, $USER"
```

#### Invoke the function

```sh
echo "Hello, $USER" | faas-cli invoke figlet
```

### Creating your own function

Create a middleware from template

```sh
faas-cli new --lang golang-middleware --prefix <dockerid> go-middle
```

This will create a yaml file for the deployment and a directory with the go file for the handler you can modify.

#### Deploy the function to openfaas

```sh
faas-cli up -f go-middle.yml
```
