package function

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/google/uuid"
)

// Handle - handles the  inbound http request
func Handle(w http.ResponseWriter, r *http.Request) {
	/*	var input []byte

		if r.Body != nil {
			defer r.Body.Close()

			body, _ := ioutil.ReadAll(r.Body)

			input = body
		}
	*/
	w.Header().Add("X-Served-Date", time.Now().String())
	w.Header().Add("Content-Type", "application/json")
	//w.Write([]byte(fmt.Sprintf("Body: %s\n", string(input))))
	id := generateUUID()
	log.Print("uuid: ", id)
	j, err := json.Marshal(id)
	if err != nil {
		w.Write([]byte(err.Error()))

	}

	w.WriteHeader(http.StatusOK)
	w.Write(j)
}

// generateUUID - returns a uuid
func generateUUID() string {
	return uuid.NewString()
}
